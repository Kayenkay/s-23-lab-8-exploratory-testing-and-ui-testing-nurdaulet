## Solution

My website is [RuTube](https://rutube.ru/)

| Test Tour           | Lab8                          |
|---------------------|-------------------------------|
| Object to be tested | Query content is displaying   |
| Test Duration       | 6.643360137939453 s           |
| Tester              | Nurdaulet Kunkhozhaev         |

| N   | What done                                 | Status | 
|-----|-------------------------------------------|--------|
| 1   | Check if title equals to Rutube title     | OK     |
| 2   | Find search input box                     | OK     |
| 3   | Insert search query                       | OK     |
| 4   | Press the search button                   | OK     |
| 5   | Get and see the content                   | OK     |