import time

from selenium import webdriver

from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

options = Options()
options.add_experimental_option("detach", True)

driver = webdriver.Chrome(options=options)
driver.implicitly_wait(10)  # seconds

start = time.time()

driver.get("https://rutube.ru/")
print("Page title is: " + driver.title)
assert "Видеохостинг RUTUBE. Смотрите видео онлайн, бесплатно" in driver.title

elem = driver.find_element(By.TAG_NAME, "input")
elem.clear()
elem.send_keys("Павел Воля")
elem.send_keys(Keys.RETURN)

elem = driver.find_element(By.CLASS_NAME, "freyja_char-infinite-scroll__scroll__6UjgN")
assert elem.is_displayed()

end = time.time()

print(end - start)
driver.close()
